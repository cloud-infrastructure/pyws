%global		srcname	pyws
%global		desc	Pyws is a project, which purpuse is to help developers to expose some\
functions of their systems as public APIs via SOAP with WSDL description.\
The main idea is to let developers completely forget about SOAP itself and\
make creating of APIs fast and painless.

Name:		python-pyws
Version:	1.1.5
Release:	4%{?dist}
Summary:	Python SOAP server providing WSDL

Group:		Development/Languages
License:	ASL 2.0
BuildArch:	noarch
URL:            https://github.com/stepank/pyws
Source0:	%{name}-%{version}.tar.gz

Requires:	python3-lxml
Requires:	python3-setuptools

BuildRequires:	python3-devel
BuildRequires:	python3-setuptools

%{?python_enable_dependency_generator}

%description
%{desc}

%package -n python3-%{srcname}
Summary:	%{summary}
%{?python_provide:%python_provide python3-%{srcname}}

%description -n python3-%{srcname}
%{desc}

%prep
%autosetup -p1 -n %{name}-%{version}

%build
%py3_build

%install
%py3_install

%files -n python3-%{srcname}
%{python3_sitelib}/%{srcname}/
%{python3_sitelib}/%{srcname}-*-py*.egg-info/

%changelog
* Mon Apr 24 2023 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.5-5
- Rebuild for RHEL and ALMA

* Tue Apr 13 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.5-4
- Rebuild for CentOS 8 Stream

* Thu Jun 18 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.5-3.el8
- Fix another python3 compatibility issue

* Wed Jun 17 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.5-2.el8
- Fix python3 compatibility issues

* Wed May 06 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.5-1.el8
- Rebuild for CentOS 8

* Wed Jan 21 2015 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch>1.1.5-1.el7
- CentOS 7 version

* Thu Feb 13 2014 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.5-1.slc6
- Bump to version 1.1.5 from upstream

* Thu Feb 07 2013 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.2-1.ai6
- Initial version of the library taken from https://github.com/stepank/pyws
